import React from 'react';
import { Task } from './Task';
import localforage from 'localforage';
export class TaskInput extends React.Component{

    constructor(props) { 
        super(props);
        this.state = {
              todo: [],
              done: []
        };
        this.updateSession = this.updateSession.bind(this);
        this.cacheStorage = this.cacheStorage.bind(this);
        
      }
    componentWillMount(){
        this.updateSession();
    }
    cacheStorage(){
        localforage.setItem("todo",this.state.todo);
        localforage.setItem("done",this.state.done);
    }
    updateSession = () => {
        localforage.getItem('todo').then((value)=>{
            console.log('TODO LocalStore');
            console.log(value);
            this.setState({todo:value})
        }).catch(function(err) {
            console.log(err);
        });        
        localforage.getItem('done').then((value)=>{
            console.log('DONE LocalStore');
            console.log(value);
            this.setState({done:value})
        }).catch(function(err) {
            console.log(err);
        });      
    }
    addTodo(something){
        this.state.todo.push(something);
        this.cacheStorage();
        this.setState({ somethingIsAboutToHappen: true })
    }
    render() {
        let input;
        return (
            <div style={{maxWidth:'700px',margin: 'auto'}}>
                <form onSubmit={(e) => {e.preventDefault();
                                        this.addTodo(input.value);
                                        input.value = '';
                                        }}>
                    <input type="text" 
                        style={{display:'table-cell', width:'100%', minHeight:'50px'}}
                        className="form-control col-md-12" 
                        ref={node => {input = node; }} 
                        placeholder="What to do....."
                        />
                    <br />
                </form>
                <Task todo={this.state.todo ? this.state.todo : [] } done={this.state.done ?  this.state.done : []}/> 
            </div>
                
            

        );
    }

}
