import React from 'react';
import AppBar from 'material-ui/AppBar';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import IconButton from 'material-ui/IconButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import localforage from 'localforage';


export class Task extends React.Component{
    cacheStorage(){
        localforage.setItem("todo",this.props.todo);
        localforage.setItem("done",this.props.done);
    }
    handleDone(something, index) {
        this.props.todo.splice(index, 1);
        this.props.done.push(something);
        this.cacheStorage();
        this.setState({ somethingIsAboutToHappen: false })
    }
    handleDoThis(something, index) {
        this.props.done.splice(index, 1);
        this.props.todo.push(something);
        this.cacheStorage();
        this.setState({ somethingIsAboutToHappen: true })
    }
    handleDeleteTodo(index) {
        this.props.todo.splice(index, 1);
        this.cacheStorage();
        this.setState({ somethingIsAboutToHappen: false })
    }
    handleDeleteDone(index) {
        this.props.done.splice(index, 1);
        this.cacheStorage();
        this.setState({ somethingIsAboutToHappen: false })
    }
    render() {
        console.log('Session TODO');
        console.log(this.props.todo);
        console.log('Session DONE');
        console.log(this.props.done);
        return (
            <div>
            <Card>
            {this.props.todo ? this.props.todo.map((todo,index) =>{
                return (
                    <div>
                        <AppBar 
                            title={<span>{todo}</span>}
                            iconElementRight={<FloatingActionButton mini={true}><NavigationClose /></FloatingActionButton>}
                            iconElementLeft={<RaisedButton label="Mark as done" />}
                            onRightIconButtonClick={()=>{this.handleDeleteTodo(index)}}
                            onLeftIconButtonClick={()=>{this.handleDone(todo, index)}}
                            
                        />
                    </div>
                );
            })
            : ''}
            </Card>
            {/* <Divider /> */}
            {this.props.done ? this.props.done.map((done,index) =>{
                return (
                    <div>
                        <AppBar 
                            title={<span>{done}</span>}
                            iconElementRight={<FloatingActionButton mini={true} style = {{backgroundColor:"#92a8d1"}}><NavigationClose /></FloatingActionButton>}
                            iconElementLeft={<RaisedButton label="Redo this task" />}
                            onRightIconButtonClick={()=>{this.handleDeleteDone(index)}}
                            onLeftIconButtonClick={()=>{this.handleDoThis(done, index)}}
                            style = {{backgroundColor:"#92a8d1"}}
                            
                        />
                    </div>
                );
            })
            : ''}
            </div>
        );
        
    }

}
