Author: Jan Daniel Laborada
Date: June 14, 2018

#SimpleTodoListApp

## Setup

###Environment
####Node Version : 6.14.3
####Yarn Version : 1.7.0

### For Unix
#### `cd` to `/SimpleTodoListApp` and run `yarn install`.
#### run `yarn start` to start the application
#### open your browser, go to `localhost:3000`